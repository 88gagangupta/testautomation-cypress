# Testautomation

## Introduction
This project is to automate the execution of end-to-end test cases including UI Tests and API Tests. Test execution report is available here: https://88gagangupta.gitlab.io/testautomation-cypress/

## Setup
Clone the repository and execute:

`npm install`

## Execution


### How to run test local on your laptop

You can run test local on your laptop.

Run the tests by:
```
npm run cy:test
```

This will start execution of all test cases on your local machine.

### Run tests in gitlab

Execution on GITLAB pipeline is triggered on any commit. It executs API Testcases followed by UI Testcases and generates HTML report. Execution is done in a docker container.

### Test Run
Other commands to execute testcases

- To execute all API test cases

    `npm run cy:apitest`

- To execute all UI test cases

    `npm run cy:uitest`

- To execute all test cases in docker container

    `docker build --no-cache -t <<give-image-name>> .`

- To execute perfromance test cases

    `jmeter -n -t performance/tmdb-performance.jmx -l ./testresults.log -e -o ./testresults -Jload=50 -JrampUp=$10 -Jhold=10 -JaccessToken=${ACCESS_TOKEN}`


## Reporting
HTML reports can be generated after completion of test execution.


To generate HTML report test cases need to be run with this command

`npm run report:allure`

This will generate HTML report in `allure-report`.

## Writing a test case

Write a under `cypress/integration`
