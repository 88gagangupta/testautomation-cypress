FROM cypress/browsers:node12.14.1-chrome85-ff81

WORKDIR /app

COPY ./cypress ./cypress
COPY ./cypress.json ./cypress.json
COPY ./cypress.env.json ./cypress.env.json
COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json

RUN npm install
RUN npm run cy:uitest
RUN npm run cy:apitest
RUN npm run report:allure