/// <reference types="cypress" />


// This file contains test scenarios for Login feature

describe('Login Feature', () => {
    beforeEach(() => {
      // Launch webpage before each test scenario
      cy.visit(' https://wave-trial.getbynder.com/login/')
    })
  
    it('should redirect to dashboard on successful login', () => {
      //Find Username text field by id and type a valid username
      cy.get('#inputEmail')
      .type('gagandeep.gupta@testingxperts.com')
      .should('has.value','gagandeep.gupta@testingxperts.com')

      //Find Password text field by id and type a valid password
      cy.get('#inputPassword')
      .type('gagan@bynder')
      .should('has.value','gagan@bynder')

      //Click Login Button
      cy.get('[type="Submit"]')
      .click()

      //Assert that user is redirected to Dashboard 'account/dashboard'
      cy.url()
      .should('include', '/account/dashboard')

      // Assert logged-in user name
      cy.get('.profile')
      .should('contains.text','Gagandeep Gupta')

    })

    it('should redirect to dashboard on successful login with Enter button', () => {
      //Find Username text field by id and type a valid username
      cy.get('#inputEmail')
      .type('gagandeep.gupta@testingxperts.com')
      .should('has.value','gagandeep.gupta@testingxperts.com')

      //Find Password text field by id and type a valid password
      cy.get('#inputPassword')
      .type('gagan@bynder')
      .should('has.value','gagan@bynder')
      .type('{enter}')

      //Assert that user is redirected to Dashboard 'account/dashboard'
      cy.url()
      .should('include', '/account/dashboard')

      // Assert logged-in user name
      cy.get('.profile')
      .should('contains.text','Gagandeep Gupta')

    })

    it('should redirect to Login page on successful logout', () => {
      //Find Username text field by id and type a valid username
      cy.get('#inputEmail')
      .type('gagandeep.gupta@testingxperts.com')

      //Find Password text field by id and type a valid password
      cy.get('#inputPassword')
      .type('gagan@bynder')

      //Click Login Button
      cy.get('[type="Submit"]')
      .click()

      // CLick Profile button
      cy.get('.profile').click()

      // Click Logout button
      cy.contains('Logout')
      .should('be.visible')
      .click()

      // Assertions ===================================================
      // Assert that user is redirected to Dashboard 'account/dashboard'
      cy.url()
      .should('include', '/login')

      // Assert that logout message is correct
      cy.get('.cbox_messagebox')
      .should('be.visible')
      .and('have.text','You have successfully logged out.')

      // Assert that logout notification is correct
      cy.get('.notification h1')
      .should('be.visible')
      .and('have.text','You have successfully logged out.')
    })

    it('should give error on invalid login', () => {
      //Find Username text field by id and type a valid username
      cy.get('#inputEmail')
      .type('gagandeep.gupta@testingxperts.com')

      //Find Password text field by id and type a valid password
      cy.get('#inputPassword')
      .type('invalid@bynder')

      //Click Login Button
      cy.get('[type="Submit"]')
      .click()

      // Assertions ====================================================
      // Assert that incorrect login message is correct
      cy.get('.cbox_messagebox')
      .should('be.visible')
      .and('have.text','You have entered an incorrect username or password.')

      // Assert that incorrect login message notification is correct
      cy.get('.notification h1')
      .should('be.visible')
      .and('have.text','You have entered an incorrect username or password.')

      // Assert the url of login page after redirect
      cy.url()
      .should('include', '/login')
    })

    it('should give error on login with empty username and password', () => {

      //Click Login Button without adding username and password
      cy.get('[type="Submit"]')
      .click()

      //Click Login Button without adding username and password
      cy.get('[type="Submit"]')
      .click()

      // Assertions ====================================================
      // Assert that incorrect login message is correct
      cy.get('.cbox_messagebox')
      .should('be.visible')
      .and('have.text','You have entered an incorrect username or password.')

      // Assert that incorrect login message notification is correct
      cy.get('.notification h1')
      .should('be.visible')
      .and('have.text','You have entered an incorrect username or password.')

      // Assert the url of login page after redirect
      cy.url()
      .should('include', '/login')
    })

    it('should redirect to forgot password on click "Lost Password?" link', () => {

      // Click "Lost Password?" link
      cy.get('.lost-password a')
      .should('be.visible')
      .click()
      
      // Assert that user is redirected to forgot password page
      cy.url()
      .should('include', '/user/forgotPassword/?redirectToken=')
    })

    it('should open "Support" flyout on click support button', () => {
      
      // Click on "Support" Button
      cy.get('#custom-support-form-button')
      .should('be.visible')
      .click()

      // Assertions =========================================================
      // Assert that "Support" form is visible
      cy.get(".modal")
      .should('be.visible')

      // Assert that title of "Support" for is correct
      cy.get(".modal .clearfix h1")
      .should('have.text','Support')
    })

    it('should open "Language" list on click', () => {

      // Click "Language" button on login page
      cy.get('.admin-options')
      .should('be.visible')
      .click()

      // Assertions ============================================================
      // Assert Language list item contains "Netherlands"
      cy.get('.admin-options')
      .find('[value="nl_NL"]').next()
      .should('contains.text','Nederlands')

      // Assert Language list item contains "English"
      cy.get('.admin-options')
      .find('[value="en_US"]').next()
      .should('contains.text','English')

      // Assert Language list item contains "Français"
      cy.get('.admin-options')
      .find('[value="fr_FR"]').next()
      .should('contains.text','Français')

      // Assert Language list item contains "Deutsch"
      cy.get('.admin-options')
      .find('[value="de_DE"]').next()
      .should('contains.text','Deutsch')

      // Assert Language list item contains "Italiano"
      cy.get('.admin-options')
      .find('[value="it_IT"]').next()
      .should('contains.text','Italiano')

      // Assert Language list item contains "Español"
      cy.get('.admin-options')
      .find('[value="es_ES"]').next()
      .should('contains.text','Español')

    })

  })
  