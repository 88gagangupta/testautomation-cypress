/// <reference types="cypress" />

describe('Testing GET /movie/top_rated API Endpoints', () => {

  const accessToken = Cypress.env('accessToken');
  const baseUrl = Cypress.env('baseUrl');

  it('Success 200: GET top rated moves', () => {
    cy.log(accessToken)
    cy.request({
      method: 'GET',
      url: baseUrl+'/movie/top_rated',
      qs: {
        api_key: accessToken,
        language: 'en-US',
        page: 1
      }
    })
      .then((response) => {
        assert.equal(response.status, 200)
        assert.isAbove(response.body.results.length, 0)
      })
  })

  it('Unauthorised 401: GET top rated moves', () => {
    cy.request({
      method: 'GET',
      url: baseUrl+'/movie/top_rated',
      failOnStatusCode: false,
      qs: {
        api_key: 'invalid_tokem',
        language: 'en-US',
        page: 1
      }
    })
      .then((response) => {
        assert.equal(response.status, 401)
        assert.equal(response.body.status_message, 'Invalid API key: You must be granted a valid key.')
      })
  })

  it.skip('Bad Request 400: GET top rated moves', () => {
    cy.request({
      method: 'GET',
      url: baseUrl+'/movie/top_rated?api_key=4c7957f26c4653ba9d28f254404815e0&language=en-US&page=one',
      failOnStatusCode: false,
      qs: {
        api_key: accessToken,
        language: 'en-US',
        page: 1
      }
    })
      .then((response) => {
        assert.equal(response.status, 400)
      })
  })

  it('Not Found 404: GET top rated moves', () => {
    cy.request({
      method: 'GET',
      url: baseUrl+'/movie/top_ratede?api_key=4c7957f26c4653ba9d28f254404815e0&language=en-US&page=1',
      failOnStatusCode: false,
      qs: {
        api_key: accessToken,
        language: 'en-US',
        page: 1
      }
    })
      .then((response) => {
        assert.equal(response.status, 404)
        assert.equal(response.body.status_message, 'The resource you requested could not be found.')
      })
  })

})