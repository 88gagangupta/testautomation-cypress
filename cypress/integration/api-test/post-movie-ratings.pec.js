/// <reference types="cypress" />

describe('Testing POST /movie/19404/rating API Endpoints', () => {
    
    const accessToken = Cypress.env('accessToken');
    const baseUrl = Cypress.env('baseUrl');

    before(() => {
        cy.getSessionId(accessToken)
    });

    it('Success 201: POST /movie/19404/rating', () => {
        const sessionId = Cypress.env('sessionId');
        const options = {
            method: 'POST',
            url: baseUrl+'/movie/19404/rating',
            qs: {
                api_key: accessToken,
                guest_session_id: sessionId
            },
            body: {
                "value": 8.5
            }
        };
        cy.request(options)
            .then((response) => {
                assert.equal(response.status, 201)
                assert.equal(response.body.status_message, 'Success.')
            })
    })

    it('Unauthorized 401: POST /movie/19404/rating', () => {
        const sessionId = Cypress.env('sessionId');
        const options = {
            method: 'POST',
            url: baseUrl+'/movie/19404/rating',
            qs: {
                api_key: 'invalidToken',
                guest_session_id: sessionId
            },
            body: {
                "value": 8.5
            },
            failOnStatusCode: false,
        };
        cy.request(options)
            .then((response) => {
                assert.equal(response.status, 401)
                assert.equal(response.body.status_message, 'Invalid API key: You must be granted a valid key.')
            })
    })

    it('Bad Request 400: POST /movie/19404/ratings', () => {
        const sessionId = Cypress.env('sessionId');
        const options = {
            method: 'POST',
            url: baseUrl+'/movie/19404/rating',
            qs: {
                api_key: accessToken,
                guest_session_id: sessionId
            },
            body: {
                "value": -1
            },
            failOnStatusCode: false,
        };
        cy.request(options)
            .then((response) => {
                assert.equal(response.status, 400)
                assert.equal(response.body.status_message, 'Value too low: Value must be greater than 0.0.')
            })
    })

    it('Not Found 404: POST /movie/19404/ratings', () => {
        const sessionId = Cypress.env('sessionId');
        const options = {
            method: 'POST',
            url: baseUrl+'/movie/19404/ratings',
            qs: {
                api_key: accessToken,
                guest_session_id: sessionId
            },
            body: {
                "value": 8.5
            },
            failOnStatusCode: false,
        };
        cy.request(options)
            .then((response) => {
                assert.equal(response.status, 404)
                assert.equal(response.body.status_message, 'The resource you requested could not be found.')
            })
    })

    it.skip('Method Not Allowed 405: POST /movie/19404/ratings', () => {
        const sessionId = Cypress.env('sessionId');
        const options = {
            method: 'GET',
            url: baseUrl+'/movie/19404/rating',
            qs: {
                api_key: accessToken,
                guest_session_id: sessionId
            },
            body: {
                "value": 8.5
            },
            failOnStatusCode: false,
        };
        cy.request(options)
            .then((response) => {
                assert.equal(response.status, 405)
            })
    })

    it.skip('Not Acceptable 406: POST /movie/19404/ratings', () => {
        const sessionId = Cypress.env('sessionId');
        const options = {
            method: 'POST',
            url: baseUrl+'/movie/19404/rating',
            qs: {
                api_key: accessToken,
                guest_session_id: sessionId
            },
            body: {
                "value": 8.5
            },
            headers:{
                'content-type': 'application/xml'
            },
            failOnStatusCode: false,
        };
        cy.request(options)
            .then((response) => {
                assert.equal(response.status, 406)
            })
    })
});